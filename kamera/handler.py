"""Handler classes."""
from picamera.array import PiMotionAnalysis
import numpy as np


class MotionDetector(PiMotionAnalysis):
    """In essence a wrapper class for analyzing motion from PiCamera."""

    def __init__(self, camera, queue_size: int=10, motion_threshold: float=4.0) -> None:
        """Setup motion detector.

        Args:
            camera:             a picamera instance.
            queue_size:         number of consecutive frames to analyze (default 10).
            motion_threshold:   minimum average motion required to call it a motion.
        """
        self.QUEUE_SIZE = queue_size
        self.THRESHOLD = motion_threshold
        super(MotionDetector, self).__init__(camera)
        self.queue = np.zeros(self.QUEUE_SIZE, dtype=np.float)
        self.motion_detected = False

    def analyze(self, a: np.ndarray) -> None:
        """Analyze a frame for motion.

        Args:
            a:      numpy array for current frame
        """
        # Cycle everything and overwrite 1st element
        self.queue = np.roll(self.queue, 1)
        self.queue[0] = np.sqrt(
            np.square(a['x'].mean()) + np.square(a['y'].mean())
        )
        mean = self.queue.mean()
        self.motion_detected = mean >= self.THRESHOLD
