import argparse
import json
import time

from dropbox import Dropbox
import picamera

from handler import MotionDetector


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-c', '--conf', required=True,
                    help='path to the JSON config file')

    args = vars(ap.parse_args())
    conf = json.load(open(args['conf']))

    dropbox = Dropbox(conf['dropbox_access_token'])

    with picamera.PiCamera(resolution=conf['camera']['resolution'],
                           framerate=conf['camera']['fps']) as camera:
        with MotionDetector(camera=camera,
                            queue_size=conf['detector']['queue_size'],
                            motion_threshold=conf['detector']['threshold']) as detector:
            stream = picamera.PiCameraCircularIO(camera, seconds=10)
            camera.start_recording(stream, format='h264', motion_output=detector)
            try:
                while True:
                    camera.wait_recording(1)
                    if detector.motion_detected:
                        detection_str = time.strftime('%Y-%m-%d %H:%M:%S %z', time.gmtime())
                        print('Motion detected, splitting')
                        camera.split_recording('{0} after.h264'.format(detection_str))
                        stream.copy_to('{0} before.h264'.format(detection_str), seconds=10)
                        stream.clear()
                        # Wait until motion ends
                        print('waiting till motion ends')
                        time.sleep(2)  # don't end so fast
                        while detector.motion_detected:
                            camera.wait_recording(1)
                        # Motion finished, return
                        print('not moving anymore')
                        camera.split_recording(stream)
            finally:
                camera.stop_recording()
