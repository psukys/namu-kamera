Namų kamera
==========================

Raspberry Pi kameros projektas paremtas RPi gidais kaip naudotis kamera. Pirminė kameros funkcija - judesio daviklis ir įrašinėtojas judant.

## Įrašymas

Visus paketus turėtų surašyti `pip` įrankis. Reikia paleisti komandą:

```
pip install .
```

## Testai

Testai gali būti paleisti su šia komanda:
```
python setup.py test
```
