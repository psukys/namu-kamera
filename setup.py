import setuptools


setuptools.setup(name='namu-kamera',
                 version='1.0.0',
                 description='Namu kameros su RPi projektas',
                 long_description=open('README.md').read().strip(),
                 author='Paulius Sukys',
                 author_email='paul.sukys@gmail.com',
                 url='https://gitlab.com/psukys/namu-kamera',
                 py_modules=['kamera'],
                 license='MIT License',
                 zip_safe=False,
                 keywords='kamera',
                 install_requires=[
                    'dropbox',
                    'picamera',
                    'numpy'
                 ],
                 setup_requires=[
                     'pytest-runner'
                 ],
                 tests_require=[
                    'pytest',
                    'pytest-cov',
                 ])
